# JUnit 5 automation #

An example project for demonstration of *JUnit 5* automation via *Gradle* and *Maven*.
The project can run unit test, generate a coverage report in
[JaCoCo](http://www.jacoco.org/) and publish
results to [SonarQube](https://www.sonarqube.org/).

## How to run the example

1. Clone the repository.
1. Run *Gradle* and *Maven* commands described below.
1. Browse the source code.

## Gradle project ##

### Run unit tests ###

Run the following command:

    $ gradle clean test

You should see following result on the console:

    :clean
    :compileJava
    :processResources NO-SOURCE
    :classes
    :compileTestJava
    :processTestResources NO-SOURCE
    :testClasses
    :junitPlatformTest

    Test run finished after 322 ms
    [         9 containers found      ]
    [         0 containers skipped    ]
    [         9 containers started    ]
    [         0 containers aborted    ]
    [         9 containers successful ]
    [         0 containers failed     ]
    [        17 tests found           ]
    [         0 tests skipped         ]
    [        17 tests started         ]
    [         0 tests aborted         ]
    [        17 tests successful      ]
    [         0 tests failed          ]

    :test SKIPPED

    BUILD SUCCESSFUL in 15s
    4 actionable tasks: 4 executed

### Generate JaCoCo code coverage report ###

Run the following command:

    $ gradle jacocoTestReport

You should find the *JaCoCo* report in the `build/reports/jacoco/test/html` directory.

### Publish results to SonarQube ###

Run the following command:

    $ gradle sonarqube

You should find the project analysis on your local *SonarQube* instance ([http://localhost:9000]()).

Having a running local *SonarQube* instance is a pre-requisite for this task. Go to
[download it](https://www.sonarqube.org/#downloads) in case you don't have it yet.

In case of remote *SonarQube* instance, please configure the *Gradle SonarQube plugin* accordingly.
See the [Configure the Scanner](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner+for+Gradle#AnalyzingwithSonarQubeScannerforGradle-ConfiguretheScanner)
section in the documentation.

## Maven project ##

### Run unit tests ###

Run the following command:

    $ mvn clean test --quiet

You should see following result on the console:

    -------------------------------------------------------
     T E S T S
    -------------------------------------------------------
    Running cz.swsamuraj.junit5.ArgsTest
    Tests run: 9, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.315 sec - in cz.swsamuraj.junit5.ArgsTest
    Running cz.swsamuraj.junit5.logic.SimpleCalculatorTest
    Tests run: 8, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.041 sec - in cz.swsamuraj.junit5.logic.SimpleCalculatorTest

    Results :

    Tests run: 17, Failures: 0, Errors: 0, Skipped: 0

### Generate JaCoCo code coverage report ###

Run the following command:

    $ mvn jacoco:report

You should find the *JaCoCo* report in the `target/site/jacoco` directory.

### Publish results to SonarQube ###

Run the following command:

    $ mvn sonar:sonar

You should find the project analysis on your local *SonarQube* instance ([http://localhost:9000]()).

Having a running local *SonarQube* instance is a pre-requisite for this task. Go to
[download it](https://www.sonarqube.org/#downloads) in case you don't have it yet.

In case of remote *SonarQube* instance, please configure the *Maven SonarQube plugin* accordingly.
See the [Global Settings](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner+for+Maven#AnalyzingwithSonarQubeScannerforMaven-GlobalSettings)
section in the documentation.

## What if you're sitting behind a proxy?

### Gradle settings ###

You need to modify (or create) your `$HOME/.gradle/gradle.properties` in
following way:

```
#!properties
systemProp.http.proxyHost=<your-proxy-host>
systemProp.http.proxyPort=<your-proxy-port>
systemProp.http.nonProxyHosts=localhost|<other-non-proxy-host>
systemProp.https.proxyHost=<your-proxy-host>
systemProp.https.proxyPort=<your-proxy-port>
systemProp.https.nonProxyHosts=localhost|<other-non-proxy-host>
```

### Maven settings ###

You need to modify (or create) your `$HOME/.m2/settings.xml` in
following way:

```
#!xml
<proxies>
    <proxy>
        <id>proxy</id>
        <active>true</active>
        <protocol>http</protocol>
        <host>your-proxy-host</host>
        <port>your-proxy-port</port>
        <nonProxyHosts>localhost|other-non-proxy-host</nonProxyHosts>
    </proxy>
</proxies>
```

## License ##

The **blog-junit5** project is published under [BSD 3-Clause](http://opensource.org/licenses/BSD-3-Clause) license.
