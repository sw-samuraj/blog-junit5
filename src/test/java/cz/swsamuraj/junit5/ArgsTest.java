package cz.swsamuraj.junit5;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ArgsTest {

    @ParameterizedTest(name = "[{index}] => args = {0}")
    @MethodSource("validIntArgsProvider")
    void parseValidIntArgs(String[] args) {
        Args arguments = new Args(args);

        assertTrue(arguments.isValid());
        assertEquals(1, arguments.getFirst());
        assertEquals(2, arguments.getSecond());
    }

    static Stream<Arguments> validIntArgsProvider() {
        return Stream.of(
                (Object) new String[]{"1", "2"},
                (Object) new String[]{"1", "2", "3"}
        ).map(Arguments::of);
    }

    @ParameterizedTest(name = "[{index}] => args = {0}")
    @MethodSource("invalidArgsProvider")
    void parseInvalidArgs(String[] args) {
        Args arguments = new Args(args);

        assertFalse(arguments.isValid());
    }

    static Stream<Arguments> invalidArgsProvider() {
        return Stream.of(
                null,
                (Object) new String[]{},
                (Object) new String[]{"1"},
                (Object) new String[]{"a", "2", "3"},
                (Object) new String[]{"1", "b", "3"},
                (Object) new String[]{"a", "b", "3"}
        ).map(Arguments::of);
    }

    @Test
    void thirdCharArgIsIgnored() {
        Args args = new Args(new String[] { "1", "2", "a" });

        assertTrue(args.isValid());
    }

}