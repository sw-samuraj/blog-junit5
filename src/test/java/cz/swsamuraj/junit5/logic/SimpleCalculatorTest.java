package cz.swsamuraj.junit5.logic;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SimpleCalculatorTest {

    private Calculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new SimpleCalculator();
    }

    @ParameterizedTest(name = "[{index}] => {0} + {1} = {2}")
    @MethodSource("addNumbersProvider")
    void add(int first, int second, int expectedResult) {
        int countedResult = calculator.add(first, second);

        assertEquals(expectedResult, countedResult);
    }

    static Stream<Arguments> addNumbersProvider() {
        return Stream.of(
                Arguments.of(1, 2, 3),
                Arguments.of(2, 3, 5)
        );
    }

    @ParameterizedTest(name = "[{index}] => {0} + {1} = Exception")
    @MethodSource("limitNumbersProvider")
    void addLimitNumbers(int first, int second) {
        Throwable e = assertThrows(ArithmeticException.class, () -> {
            calculator.add(first, second);
        });

        assertEquals("integer overflow", e.getMessage());
    }

    @ParameterizedTest(name = "[{index}] => {0} * {1} = Exception")
    @MethodSource("limitNumbersProvider")
    void multiplyLimitNumbers(int first, int second) {
        Throwable e = assertThrows(ArithmeticException.class, () -> {
            calculator.multiply(first, second);
        });

        assertEquals("integer overflow", e.getMessage());
    }

    static Stream<Arguments> limitNumbersProvider() {
        return Stream.of(
                Arguments.of(Integer.MAX_VALUE, Integer.MAX_VALUE),
                Arguments.of(Integer.MIN_VALUE, Integer.MIN_VALUE)
        );
    }

    @ParameterizedTest(name = "[{index}] => {0} * {1} = {2}")
    @MethodSource("multiplyNumbersProvider")
    void multiply(int first, int second, int expectedResult) {
        int countedResult = calculator.multiply(first, second);

        assertEquals(expectedResult, countedResult);
    }

    static Stream<Arguments> multiplyNumbersProvider() {
        return Stream.of(
                Arguments.of(1, 2, 2),
                Arguments.of(2, 3, 6)
        );
    }

}