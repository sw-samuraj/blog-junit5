package cz.swsamuraj.junit5;

public class Args {

    private Integer first = null;
    private Integer second = null;

    public Args(String[] args) {
        if (args == null || args.length < 2) {
            System.out.println("It's mandatory to provide 2 arguments.");
        } else {
            first = parse(args[0]);
            second = parse(args[1]);
        }
    }

    public int getFirst() {
        return first;
    }

    public int getSecond() {
        return second;
    }

    public boolean isValid() {
        return first != null && second != null;
    }

    private Integer parse(String arg) {
        try {
            return Integer.parseInt(arg);
        } catch (NumberFormatException e) {
            System.out.printf("Provided parameter [%s] is not a number.%n", arg);

            return null;
        }
    }

}
