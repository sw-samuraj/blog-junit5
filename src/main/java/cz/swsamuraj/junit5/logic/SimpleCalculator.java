package cz.swsamuraj.junit5.logic;

public class SimpleCalculator implements Calculator {

    @Override
    public int add(int first, int second) {
        return Math.addExact(first, second);
    }

    @Override
    public int multiply(int base, int times) {
        return Math.multiplyExact(base, times);
    }

}
