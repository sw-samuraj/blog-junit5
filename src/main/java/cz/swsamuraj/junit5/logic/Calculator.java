package cz.swsamuraj.junit5.logic;

public interface Calculator {

    int add(int first, int second);

    int multiply(int base, int times);

}
