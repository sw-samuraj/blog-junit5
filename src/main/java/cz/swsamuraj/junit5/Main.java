package cz.swsamuraj.junit5;

import cz.swsamuraj.junit5.logic.Calculator;
import cz.swsamuraj.junit5.logic.SimpleCalculator;

public class Main {
    public static void main(String[] args) {
        Args arguments = new Args(args);

        if (arguments.isValid()) {
            Calculator calculator = new SimpleCalculator();
            int addResult = calculator.add(arguments.getFirst(), arguments.getSecond());
            int multiplyResult = calculator.multiply(arguments.getFirst(), arguments.getSecond());

            System.out.printf("Result of the add operation is: %d%n", addResult);
            System.out.printf("Result of the multiply operation is: %d%n", multiplyResult);
        }
    }
}
